module github.com/dtapps/go-library

go 1.16

require (
	github.com/baidubce/bce-sdk-go v0.9.73 // indirect
	github.com/bitly/go-simplejson v0.5.0
	github.com/bmizerany/assert v0.0.0-20160611221934-b7ed37b82869 // indirect
	github.com/google/uuid v1.2.0
	github.com/kr/pretty v0.2.1 // indirect
	github.com/nbio/st v0.0.0-20140626010706-e9e8d9816f32 // indirect
	github.com/nilorg/sdk v0.0.0-20210429091026-95b6cdc95c84
	golang.org/x/net v0.0.0-20210525063256-abc453219eb5 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
	gopkg.in/h2non/gentleman.v2 v2.0.5
)
