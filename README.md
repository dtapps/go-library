<img width="100" src="https://kodo-cdn.dtapp.net/04/999e9f2f06d396968eacc10ce9bc8a.png" alt="www.dtapp.net"/>

<h1><a href="https://www.dtapp.net/">Golang扩展包</a></h1>

📦 Golang扩展包

[comment]: <> (dtapps)
[![GitHub Org's stars](https://img.shields.io/github/stars/dtapps)](https://github.com/dtapps)

[comment]: <> (go)
[![golang version](https://img.shields.io/badge/golang-%3E%3D1.6-8892BF.svg)](https://pkg.go.dev/github.com/dtapps/go-library)
[![godoc](https://pkg.go.dev/badge/github.com/dtapps/go-library?status.svg)](https://pkg.go.dev/github.com/dtapps/go-library)

[comment]: <> (goproxy.cn)
[![goproxy](https://goproxy.cn/stats/github.com/dtapps/go-library/badges/download-count.svg)](https://goproxy.cn/stats/github.com/dtapps/go-library)

[comment]: <> (goreportcard.com)
[![go report card](https://goreportcard.com/badge/github.com/dtapps/go-library)](https://goreportcard.com/report/github.com/dtapps/go-library)

[comment]: <> (github.com)
[![watchers](https://badgen.net/github/watchers/dtapps/go-library)](https://github.com/dtapps/go-library/watchers)
[![stars](https://badgen.net/github/stars/dtapps/go-library)](https://github.com/dtapps/go-library/stargazers)
[![forks](https://badgen.net/github/forks/dtapps/go-library)](https://github.com/dtapps/go-library/network/members)
[![issues](https://badgen.net/github/issues/dtapps/go-library)](https://github.com/dtapps/go-library/issues)
[![branches](https://badgen.net/github/branches/dtapps/go-library)](https://github.com/dtapps/go-library/branches)
[![releases](https://badgen.net/github/releases/dtapps/go-library)](https://github.com/dtapps/go-library/releases)
[![tags](https://badgen.net/github/tags/dtapps/go-library)](https://github.com/dtapps/go-library/tags)
[![license](https://badgen.net/github/license/dtapps/go-library)](https://github.com/dtapps/go-library/blob/master/LICENSE)
[![contributors](https://badgen.net/github/contributors/dtapps/go-library)](https://github.com/dtapps/go-library/CONTRIBUTING.md)
[![GitHub go.mod Go version (subdirectory of monorepo)](https://img.shields.io/github/go-mod/go-version/dtapps/go-library)](https://github.com/dtapps/go-library)
[![GitHub release (latest SemVer)](https://img.shields.io/github/v/release/dtapps/go-library)](https://github.com/dtapps/go-library/releases)
[![GitHub tag (latest SemVer)](https://img.shields.io/github/v/tag/dtapps/go-library)](https://github.com/dtapps/go-library/tags)
[![GitHub pull requests](https://img.shields.io/github/issues-pr/dtapps/go-library)](https://github.com/dtapps/go-library/pulls)
[![GitHub issues](https://img.shields.io/github/issues/dtapps/go-library)](https://github.com/dtapps/go-library/issues)
[![GitHub Sponsors](https://img.shields.io/github/sponsors/dtapps)](https://github.com/dtapps/go-library/FUNDING.yml)
[![GitHub code size in bytes](https://img.shields.io/github/languages/code-size/dtapps/go-library)](https://github.com/dtapps/go-library)
[![GitHub language count](https://img.shields.io/github/languages/count/dtapps/go-library)](https://github.com/dtapps/go-library)
[![GitHub search hit counter](https://img.shields.io/github/search/dtapps/go-library/go)](https://github.com/dtapps/go-library)
[![GitHub top language](https://img.shields.io/github/languages/top/dtapps/go-library)](https://github.com/dtapps/go-library)

[comment]: <> (scrutinizer-ci.com)
[![Scrutinizer build (GitHub/Bitbucket)](https://img.shields.io/scrutinizer/build/g/dtapps/go-library/master)](https://scrutinizer-ci.com/g/dtapps/go-library)
[![Scrutinizer coverage (GitHub/BitBucket)](https://img.shields.io/scrutinizer/coverage/g/dtapps/go-library/master)](https://scrutinizer-ci.com/g/dtapps/go-library)
[![Scrutinizer code quality (GitHub/Bitbucket)](https://img.shields.io/scrutinizer/quality/g/dtapps/go-library/master)](https://scrutinizer-ci.com/g/dtapps/go-library)

[comment]: <> (www.travis-ci.com)
[![Travis (.com) branch](https://img.shields.io/travis/com/dtapps/go-library/master)](https://www.travis-ci.com/github/dtapps/go-library)

[comment]: <> (app.codecov.io)
[![Codecov branch](https://img.shields.io/codecov/c/github/dtapps/go-library/master)](https://app.codecov.io/gh/dtapps/go-library)

[comment]: <> (gitlab.com)
[![gitlab (.com)](https://gitlab.com/dtapps/go-library/badges/master/pipeline.svg)](https://gitlab.com/dtapps/go-library)

[comment]: <> (codechina.csdn.net)
[![codechina.csdn (.net)](https://codechina.csdn.net/dtapps/go-library/badges/master/pipeline.svg)](https://codechina.csdn.net/dtapps/go-library)

## Install

```Importing
go get github.com/dtapps/go-library
```
